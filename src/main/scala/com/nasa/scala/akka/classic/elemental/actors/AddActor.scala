package com.nasa.scala.akka.classic.elemental.actors

import akka.actor.Actor

class AddActor extends Actor{
  var sum:Int = 0
  override def receive: Receive = {

    case 7 => sum+=7
              sender ! sum
    case x:Int => sum+=x
    //  println(s"Suma actual $sum")
    case _ => println(s"Mensaje no tratado")
  }
}
