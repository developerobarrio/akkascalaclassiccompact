package com.nasa.scala.akka.classic.elemental

import akka.actor.{ActorRef, ActorSystem, Props}
import com.nasa.scala.akka.classic.elemental.actors.AddActor
import akka.util.Timeout
import akka.pattern.ask

import scala.concurrent.{Await, Future, duration}
import scala.concurrent.duration._
import scala.language.postfixOps

object ActorSetUp extends App{

  implicit val timeout = Timeout(10  seconds)
  val scene:ActorSystem = ActorSystem("TeatroReal")
  println(scene)
  val addActor:ActorRef = scene.actorOf(Props[AddActor])
  println(addActor.path)
  val future: Future[Int] = (addActor ? 7).mapTo[Int]
  addActor ! 45
  addActor ! 7
  addActor ! "Escocia"
  val result = Await.result(future, 1 seconds)
  println(s"Resultado despues del Future $result")




}
